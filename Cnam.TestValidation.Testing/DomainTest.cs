using Cnam.TestValidation.DomainService;
using Cnam.TestValidation.DomainService.CofeeUseCaseException;
using Cnam.TestValidation.Infrastructure;
using Cnam.TestValidation.Model;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Threading;
using Xunit;
using Xunit.Abstractions;

namespace Cnam.TestValidation.Testing
{
    public class DomainTest
    {
        public IDataExchange DataExchangeForTest = new InMemoryInfrastructure();   //new PostgreInfrastructure();   // // //    //   
        private readonly ITestOutputHelper output;

        public DomainTest(ITestOutputHelper outputHelper)
        {
            output = outputHelper;
        }

        [Trait("Category", "Model: User")]
        [Fact(DisplayName ="Add Multiple Users")]
        public void TestAddMultipleUsers()
        {
            CofeeUseCase cofeeUseCase = new (DataExchangeForTest);
            User userA = TestFacilitator.RandomUserGenerator();
            User userB = TestFacilitator.RandomUserGenerator();
            output.WriteLine(userA.UserName);
            output.WriteLine(userB.UserName);
            cofeeUseCase.AddUser(userA);
            cofeeUseCase.AddUser(userB);

            Assert.Equal(userA, cofeeUseCase.GetUser(userA.Guid));
            Assert.Equal(userB, cofeeUseCase.GetUser(userB.Guid));

            //clean test
            cofeeUseCase.DeleteUser(userA);
            cofeeUseCase.DeleteUser(userB);
        }

        [Trait("Category", "Model: User")]
        
        [Fact(DisplayName = "Error On Adding User Twice")]
        public void TestErrorOnAddingUserTwice()
        {
            CofeeUseCase cofeeUseCase = new (DataExchangeForTest);
            User user = TestFacilitator.RandomUserGenerator();
            cofeeUseCase.AddUser(user);
            Assert.Throws<CofeeUseCaseUserCreationException>(() => { cofeeUseCase.AddUser(user); });
        }

        [Trait("Category", "Model: User")]
        
        [Fact(DisplayName = "Ask Guid With Correct Data")]
        public void AskGuidWithCorrectData()
        {
            CofeeUseCase cofeeUseCase = new(DataExchangeForTest);
            User user = TestFacilitator.RandomUserGenerator();
            cofeeUseCase.AddUser(user);
            Guid guid = cofeeUseCase.ProvideGuidOfUser(user.UserName, user.UserPassword);
            Assert.Equal(user.Guid, guid);
        }

        [Trait("Category", "Model: User")]
        
        [Fact(DisplayName = "Ask Guid With Incorrect Data")]
        public void AskGuidWithIncorrectData()
        {
            CofeeUseCase cofeeUseCase = new(DataExchangeForTest);
            User user = TestFacilitator.RandomUserGenerator();
            cofeeUseCase.AddUser(user);
            Assert.Throws<CofeeUseCaseUserErrorUserAuthenthificationException>(() => cofeeUseCase.ProvideGuidOfUser(user.UserName, "falsePassword"));
            Assert.Throws<CofeeUseCaseUserErrorUserAuthenthificationException>(() => cofeeUseCase.ProvideGuidOfUser("falseUser", user.UserPassword));
        }


        [Trait("Category", "Model: User")]
        
        [Fact(DisplayName = "Error On Getting Inexistant User")]
        public void ErrorOnGettingInexistantUser() {
            CofeeUseCase cofeeUseCase = new (DataExchangeForTest);
            Assert.Throws<CofeeUseCaseUserAccesException>(() => { User falseUser = cofeeUseCase.GetUser(Guid.NewGuid()); });
        }
        
        [Trait("Category", "Model: Purse")]
        
        [Theory(DisplayName = "Increasing Purse Several Time")]
        [InlineData(0, 1d, 100)]
        [InlineData(0, 0.01d, 2)]
        public void IncreasingPurseSeveralTime(decimal initialValue, decimal increasingValue, int numberAttempt)
        {
            User userAsking = TestFacilitator.RandomUserGenerator();
            userAsking.AddCoins(initialValue);

            CofeeUseCase useCase = new CofeeUseCase(DataExchangeForTest);
            useCase.AddUser(userAsking);

            PurseAction add = PurseAction.Add;
            for( int attempt=0; attempt< numberAttempt; attempt++)
            {
                useCase.ActionOnPurse(add, userAsking, increasingValue);
            }
            Assert.Equal(initialValue + numberAttempt*increasingValue, useCase.GetUser(userAsking.Guid).UserPurse.CoinPurse);

            //clean test
            useCase.DeleteUser(userAsking);
        }
        
        [Trait("Category", "Model: Purse")]
        
        [Theory(DisplayName = "Decreasing Purse Several Time")]
        [InlineData(100, 1d, 100)]
        [InlineData(0.02, 0.01d, 2)]
        public void DecreasingPurseSeveralTime(decimal initialValueInPurse, decimal valueToDecrease, int attemptToRemove)
        {
            User userAsking = TestFacilitator.RandomUserGenerator();
            
            CofeeUseCase useCase = new CofeeUseCase(DataExchangeForTest);
            useCase.AddUser(userAsking);
            useCase.ActionOnPurse(PurseAction.Add, userAsking, initialValueInPurse);
            PurseAction remove = PurseAction.Remove;

            for (int attempt=0; attempt < attemptToRemove; attempt++)
            {
                useCase.ActionOnPurse(remove, userAsking, valueToDecrease);
            }

            Assert.Equal(initialValueInPurse - attemptToRemove*valueToDecrease, useCase.GetUser(userAsking.Guid).UserPurse.CoinPurse);
        }

        [Trait("Category", "Model: Purse")]
        
        [Fact(DisplayName = "Error On Attempt To Set Negative Purse")]
        public void ErrorOnAttemptToSetNegativePurse()
        {
            User user = TestFacilitator.RandomUserGenerator();
            CofeeUseCase useCase = new CofeeUseCase(DataExchangeForTest);
            useCase.AddUser(user);
            Assert.Throws<CofeeUseCaseValueException>(() => { useCase.ActionOnPurse(PurseAction.Remove, user, -0.001m); });
            Assert.Throws<CofeeUseCaseUserPurseUpdateException>(() => { useCase.ActionOnPurse(PurseAction.Remove, user, 0.001m); });
        }
        
        [Trait("Category", "Model: Item")]
        
        [Theory(DisplayName = "Create Article")]
        [InlineData("5478000267412","Coca-cola",12.50,"Une boisson gazeuse c�l�bre")]
        public void CreateArticle(string ean,string title,decimal price,string description)
        {
            Item newItem = new Item(ean,title, price, description);
            CofeeUseCase cofeeUseCase = new CofeeUseCase(DataExchangeForTest);
            cofeeUseCase.AddItem(newItem);
            Assert.Equal(newItem, cofeeUseCase.GetItem(newItem));
            Assert.Equal(newItem, cofeeUseCase.GetItem(newItem.Id));

            Item newItemWithoutOriginalId = new Item(newItem.EAN,newItem.Title,newItem.Price,newItem.Description);
            Assert.Equal(newItemWithoutOriginalId, cofeeUseCase.GetItem(newItem));
            Assert.Equal(newItemWithoutOriginalId, cofeeUseCase.GetItem(newItem.Id));
            //clean test
            cofeeUseCase.DeleteItem(newItem);
        }

        [Trait("Category", "Model: Item")]
        
        [Fact(DisplayName = "Error On Creating Same Article Twice")]
        public void ErrorOnCreatingSameArticleTwice()
        {
            Item newItem = TestFacilitator.RandomItemGenerator();
            CofeeUseCase cofeeUseCase = new CofeeUseCase(DataExchangeForTest);
            cofeeUseCase.AddItem(newItem);
            Assert.Throws<CofeeUseCaseItemCreationException>(() => { cofeeUseCase.AddItem(newItem); });

            Item newItemButWithDifferentId = new Item(newItem.EAN, newItem.Title, newItem.Price, newItem.Description);

            Assert.Throws<CofeeUseCaseItemCreationException>(() => { cofeeUseCase.AddItem(newItemButWithDifferentId); });
            //clean test
            cofeeUseCase.DeleteItem(newItem);
        }

        [Trait("Category", "Model: Item")]
        
        [Fact(DisplayName = "Update Article")]
        public void UpdateArticle()
        {
            Item originalItem = TestFacilitator.RandomItemGenerator();
            CofeeUseCase cofeeUseCase = new CofeeUseCase(DataExchangeForTest);
            cofeeUseCase.AddItem(originalItem);

            string randomTitle = Path.GetRandomFileName();
            string randomDescription = Path.GetRandomFileName();
            decimal randomPrice= TestFacilitator.ProvideRandomPrice();

            Item ItemToUpdate = new Item { 
                Id = originalItem.Id, 
                Description = randomDescription, 
                Price = randomPrice ,
                Title = randomTitle, 
                EAN = originalItem.EAN
            };

            cofeeUseCase.UpdateItem(ItemToUpdate);
            Assert.Equal(cofeeUseCase.GetItem(ItemToUpdate), ItemToUpdate);
            Assert.NotEqual(originalItem.Description,ItemToUpdate.Description);
            Assert.NotEqual(originalItem.Price, ItemToUpdate.Price);
            Assert.NotEqual(originalItem.Title, ItemToUpdate.Title);

            //clean code
            cofeeUseCase.DeleteItem(ItemToUpdate);
        }

        [Trait("Category", "Model: Item")]
        
        [Fact(DisplayName = "Error On Updating Or Getting Inexisting Item")]
        public void ErrorOnUpdatingOrGettingInexistingItem()
        {
            Item originalItem = new Item();
            CofeeUseCase cofeeUseCase = new (DataExchangeForTest);
            Assert.Throws<CofeeUseCaseItemAccesException>(() => { cofeeUseCase.UpdateItem(originalItem); });
            Assert.Throws<CofeeUseCaseItemAccesException>(() => { cofeeUseCase.GetItem(originalItem); });
        }

        [Trait("Category", "Model: Item")]
        [Fact(DisplayName = "Delete Article")]
        public void DeleteArticle()
        {
            Item itemToDelete = TestFacilitator.RandomItemGenerator();
            Item itemNotToDelete = TestFacilitator.RandomItemGenerator();
            CofeeUseCase cofeeUseCase = new (DataExchangeForTest);
            cofeeUseCase.AddItem(itemToDelete);
            cofeeUseCase.AddItem(itemNotToDelete);
            cofeeUseCase.DeleteItem(itemToDelete);

            Assert.Throws<CofeeUseCaseItemAccesException>(() => { cofeeUseCase.GetItem(itemToDelete); });
            Assert.Equal(itemNotToDelete,cofeeUseCase.GetItem(itemNotToDelete));
        }

        [Trait("Category", "Model: Item")]
        
        [Fact(DisplayName = "Error On Deleting Inexisting Item")]
        public void ErrorOnDeletingInexistingItem()
        {
            Item originalItem = TestFacilitator.RandomItemGenerator();
            CofeeUseCase cofeeUseCase = new(DataExchangeForTest);
            Assert.Throws<GenericUseCaseException>(() => { cofeeUseCase.DeleteItem(originalItem); });
        }

        [Trait("Category", "Model: Item")]
        
        [Fact(DisplayName ="Get All Items")]
        public void GetAllItems()
        {
            CofeeUseCase cofeeUseCase = new(DataExchangeForTest);
            List<Item> expectedItemList = new();
            expectedItemList.AddRange(cofeeUseCase.GetAllItem());
            for (int i =0; i < 10; i++)
            {
                Item randomItem = TestFacilitator.RandomItemGenerator();
                expectedItemList.Add(randomItem);
                cofeeUseCase.AddItem(randomItem);
            }

            List<Item> listFromUseCase = cofeeUseCase.GetAllItem();
            foreach (Item item in expectedItemList)
            {
                Assert.Contains(item, listFromUseCase);
            }
        }

        [Trait("Category", "Model: Request")]
        
        [Fact(DisplayName = "Create And Get Request Item With Enougth Purse Value")]
        public void CreateAndGetRequestItemWithEnougthPurseValue()
        {
            Item randomItemToRequest = TestFacilitator.RandomItemGenerator();
            int quantityRequested = TestFacilitator.ProvideRandomInt();
            User userAsking = TestFacilitator.RandomUserGenerator();
            CofeeUseCase useCase = new CofeeUseCase(DataExchangeForTest);
            useCase.AddItem(randomItemToRequest);
            useCase.AddUser(userAsking);

            output.WriteLine("userAsking:" + userAsking.ToString());
            output.WriteLine("randomItemToRequest:" + randomItemToRequest.ToString());
            output.WriteLine("quantityRequested:" + quantityRequested.ToString());

            useCase.ActionOnPurse(PurseAction.Add,userAsking,quantityRequested*randomItemToRequest.Price);
            userAsking = useCase.GetUser(userAsking.Guid);

            Guid idCreatedRequest= useCase.CreateRequestItemOfUser(userAsking, randomItemToRequest, quantityRequested);
            UserItemRequest targetRequest = useCase.GetRequestWith(idCreatedRequest);

            List<UserItemRequest> listOfRequest = useCase.GetRequestWith(RequestStatus.InProgress);
            UserItemRequest requestFromBdd = listOfRequest.Find(request => request.Guid.Equals(targetRequest.Guid));
            Assert.Equal(targetRequest, requestFromBdd);

            listOfRequest = useCase.GetRequestWith(userAsking);
            foreach (var r in listOfRequest) output.WriteLine(r.User.UserName.ToString());
            requestFromBdd = listOfRequest.Find(r => r.Equals(targetRequest));
            ;
            Assert.Equal(targetRequest, requestFromBdd);

            requestFromBdd = useCase.GetRequestWith(targetRequest.Guid);
            Assert.Equal(targetRequest, requestFromBdd);

            Assert.True(useCase.GetUser(userAsking.Guid).UserPurse.CoinPurse.Equals(0));
        }

        [Trait("Category", "Model: Request")]
        
        [Theory(DisplayName = "Several Request Item Decrease Purse Value Of User")]
        [InlineData(100,25,4,1,0)]
        [InlineData(1, 0.10d, 2, 5, 0)]
        public void RequestItemDecreasePurseValueOfUser(decimal initialPruse, decimal costItem, int numberOfRequest, int quantityItemRequestedPerRequest, decimal expectedPurseValueAtTheEnd)
        {
            Item item = TestFacilitator.RandomItemGenerator(costItem);
            User randomUserAsking = TestFacilitator.RandomUserGenerator();
            CofeeUseCase useCase = new CofeeUseCase(DataExchangeForTest);
            useCase.AddUser(randomUserAsking);
            useCase.AddItem(item);
            useCase.ActionOnPurse(PurseAction.Add, randomUserAsking, initialPruse);
            
            for (int iRequestNumber = 0; iRequestNumber < numberOfRequest; iRequestNumber++)
            {
                randomUserAsking = useCase.GetUser(randomUserAsking.Guid);
                useCase.CreateRequestItemOfUser(randomUserAsking, item, quantityItemRequestedPerRequest);
            }

            Assert.Equal(expectedPurseValueAtTheEnd,useCase.GetUser(randomUserAsking.Guid).UserPurse.CoinPurse);

        }

        [Trait("Category", "Model: Request")]
        
        [Fact(DisplayName = "Error On Request Inexistant Item Or User Or Not Enought Purse Value")]
        public void ErrorOnRequestInexistantItemOrUserOrNotEnoughtPurseValue()
        {
            decimal PriceOfItem = 1_000m;
            Item randomItemToRequest = TestFacilitator.RandomItemGenerator(PriceOfItem);
            int quantityRequested = TestFacilitator.ProvideRandomInt();
            User randomUserAsking = TestFacilitator.RandomUserGenerator();
            CofeeUseCase useCase = new CofeeUseCase(DataExchangeForTest);

            // ask when nothing is created
            Assert.Throws<CofeeUseCaseItemAccesException>(() => {
                useCase.CreateRequestItemOfUser(randomUserAsking, randomItemToRequest, quantityRequested);
            });

            // ask when only item is created
            useCase.AddItem(randomItemToRequest);
            Assert.Throws<CofeeUseCaseUserAccesException>(() => {
                useCase.CreateRequestItemOfUser(randomUserAsking, randomItemToRequest, quantityRequested);
            });

            // ask when item & user is created but purse at zero
            useCase.AddUser(randomUserAsking);
            Assert.Throws<CofeeUseCaseRequestCreationException>(() =>
            {
                useCase.CreateRequestItemOfUser(randomUserAsking, randomItemToRequest, quantityRequested);
            });

            // ask inexisting id
            Assert.Throws<CofeeUseCaseRequestAccesExeption>(() =>
            {
                useCase.GetRequestWith(Guid.NewGuid());
            });


            // ask expensive item with not enougth value in purse
            useCase.ActionOnPurse(PurseAction.Add, randomUserAsking, 2);
            Assert.Throws<CofeeUseCaseRequestCreationException>(() =>
            {
                useCase.CreateRequestItemOfUser(randomUserAsking, randomItemToRequest, quantityRequested);
            });
        }

        [Trait("Category", "Model: Request")]
        
        [Theory(DisplayName = "Validate Request Status")]
        [InlineData(10d,2,5d)]
        public void ValidateRequestStatus(decimal initialPurse, int requestedItemQuantityPerRequest, decimal initialItemPrice)
        {
            Item randomItemToRequest = TestFacilitator.RandomItemGenerator(initialItemPrice);
            User randomUserAsking = TestFacilitator.RandomUserGenerator();
            CofeeUseCase useCase = new CofeeUseCase(DataExchangeForTest);
            useCase.AddUser(randomUserAsking);
            useCase.AddItem(randomItemToRequest);

            useCase.ActionOnPurse(PurseAction.Add, randomUserAsking, initialPurse);
            randomUserAsking = useCase.GetUser(randomUserAsking);

            Guid idRequest = useCase.CreateRequestItemOfUser(randomUserAsking, randomItemToRequest, requestedItemQuantityPerRequest);
            UserItemRequest request = useCase.GetRequestWith(idRequest);
            useCase.ValidateRequest(request);

            request = useCase.GetRequestWith(request);
            Assert.Contains(request, useCase.GetRequestWith(RequestStatus.Validated));
            Assert.Equal(request, useCase.GetRequestWith(request.Guid));
            Assert.Equal(initialPurse - requestedItemQuantityPerRequest* initialItemPrice, useCase.GetUser(randomUserAsking.Guid).UserPurse.CoinPurse);
        }

        [Trait("Category", "Model: Request")]
        
        [Fact(DisplayName = "Error On Validated Request Status")]
        public void ErrorOnValidatedRequestStatus()
        {
            int requestedItemQuantityPerRequest = TestFacilitator.ProvideRandomInt();
            Item randomItemToRequest = TestFacilitator.RandomItemGenerator(100m);
            User randomUserAsking = TestFacilitator.RandomUserGenerator();
            CofeeUseCase useCase = new CofeeUseCase(DataExchangeForTest);

            UserItemRequest request = new UserItemRequest(randomUserAsking, randomItemToRequest, requestedItemQuantityPerRequest);
            Assert.Throws<CofeeUseCaseRequestAccesExeption>(() => { useCase.ValidateRequest(request); });

        }
        [Trait("Category", "Model: Request")]
        
        [Fact(DisplayName = "Error On Wrongly Updating Request Status")]
        public void ErrorOnWronglyUpdatingRequestStatus()
        {
            CofeeUseCase useCase = new CofeeUseCase(DataExchangeForTest);
            Item randomItemToRequest = TestFacilitator.RandomItemGenerator(10m);
            int quantityRequested = TestFacilitator.ProvideRandomInt();
            User randomUserAsking = TestFacilitator.RandomUserGenerator();
            UserItemRequest request = new UserItemRequest(randomUserAsking, randomItemToRequest, quantityRequested);

            useCase.AddUser(randomUserAsking);
            useCase.AddItem(randomItemToRequest);
            useCase.ActionOnPurse(PurseAction.Add, randomUserAsking, 10);
            randomUserAsking = useCase.GetUser(randomUserAsking);

            Guid idRequestAlreadyCancel = useCase.CreateRequestItemOfUser(randomUserAsking, randomItemToRequest, 1);
            UserItemRequest requestAlreadyCancel = useCase.GetRequestWith(idRequestAlreadyCancel);
            useCase.CancelRequest(requestAlreadyCancel);

            Assert.Throws<CofeeUseCaseRequestUpdateException>(() => { useCase.ValidateRequest(requestAlreadyCancel); });
            Assert.Throws<CofeeUseCaseRequestUpdateException>(() => { useCase.CancelRequest(requestAlreadyCancel); });

            Guid idRequestAlreadyValidated = useCase.CreateRequestItemOfUser(randomUserAsking, randomItemToRequest, 1);
            UserItemRequest requestAlreadyValidated = useCase.GetRequestWith(idRequestAlreadyValidated);
            useCase.ValidateRequest(requestAlreadyValidated);

            Assert.Throws<CofeeUseCaseRequestUpdateException>(() => { useCase.ValidateRequest(requestAlreadyValidated); });
            Assert.Throws<CofeeUseCaseRequestUpdateException>(() => { useCase.CancelRequest(requestAlreadyValidated); });
        }

        [Trait("Category", "Model: Request")]
        
        [Fact(DisplayName = "Cancel Request")]
        public void CancelRequest()
        {
            decimal initialPurse = 10;
            decimal costOfOneItem = initialPurse;
            int quantityRequested = 1;

            CofeeUseCase useCase = new CofeeUseCase(DataExchangeForTest);
            Item randomItemToRequest = TestFacilitator.RandomItemGenerator(costOfOneItem);
            User randomUserAsking = TestFacilitator.RandomUserGenerator();
            useCase.AddUser(randomUserAsking);
            useCase.AddItem(randomItemToRequest);
            useCase.ActionOnPurse(PurseAction.Add, randomUserAsking, initialPurse);
            randomUserAsking = useCase.GetUser(randomUserAsking);

            Guid idRequest = useCase.CreateRequestItemOfUser(randomUserAsking, randomItemToRequest, quantityRequested);
            UserItemRequest request = useCase.GetRequestWith(idRequest);
            useCase.CancelRequest(request);
            request = useCase.GetRequestWith(idRequest);

            List<UserItemRequest> requestAsList = new List<UserItemRequest> { request };
            Assert.Contains(request, useCase.GetRequestWith(RequestStatus.Canceled));
            Assert.Equal(request, useCase.GetRequestWith(request.Guid));
            Assert.Equal(initialPurse, useCase.GetUser(randomUserAsking.Guid).UserPurse.CoinPurse);

        }
    }
}