﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebDriverManager;
using WebDriverManager.DriverConfigs.Impl;
using Xunit;
using Xunit.Gherkin.Quick;



namespace Cnam.TestValidation.Testing.Acceptance
{
    [Trait("Category", "Gerkins")]
    [FeatureFile("./Acceptance/BasicUserFeature.feature")]
    
    public class BasicUser : Feature
    {
        public string driverManager = new DriverManager().SetUpDriver(new ChromeConfig());

        
        public ChromeDriver driver = new ChromeDriver();

        [Given(@"I navigate to the web login page")]
        public void I_navigate_to_the_web_login_page()
        {
            driver.Url = "http://localhost:4200/";
        }

        [And(@"I input my login id")]
        public void I_input_my_login_id()
        {
            driver.FindElement(By.Id("login")).SendKeys("selenium");
            driver.FindElement(By.Id("password")).SendKeys("selenium");
        }

        [When(@"I connect to my account")]
        public void I_connect_to_my_account()
        {
            driver.FindElement(By.XPath("//button[contains(text(),'login')]")).Click();
        }

        [Then(@"I verify the correct connection to my account")]
        public void I_verify_the_correct_connectionn_to_my_account()
        {
            IWebElement elementConnected = driver.FindElement(By.XPath("//a[contains(text(),'Connected')]"));
            Assert.True(elementConnected is not null);
        }
    }
}
