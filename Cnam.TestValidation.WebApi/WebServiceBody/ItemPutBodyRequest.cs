﻿using Cnam.TestValidation.Model;

namespace Cnam.TestValidation.WebApi.WebServiceBody
{
    public class ItemPutBodyRequest
    {
        /// <summary>
        /// Swager description
        /// </summary>
        /// <example>5de3750f-8e21-4ff8-b8b4-8dfe3bae3ae4</example>
        public Guid Id { get; set; }
        
        /// <summary>
        /// Swager description
        /// </summary>
        /// <example>5478000267412</example>
        public string EAN { get; set; }
        /// <summary>
        /// Swager description
        /// </summary>
        /// <example>Coca-Cola</example>
        public string Title { get; set; }

        /// <summary>
        /// Swager description
        /// </summary>
        /// <example>1.50</example>
        public decimal Price { get; set; }
        /// <summary>
        /// Swager description
        /// </summary>
        /// <example>Boisson gazeuse célèbre</example>
        public string Description { get; set; }

        public static implicit operator Item(ItemPutBodyRequest item)
        {
            return new Item(item.Id,item.EAN, item.Title, item.Price, item.Description);
        }
    }
}
