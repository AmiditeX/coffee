﻿using Cnam.TestValidation.Infrastructure;
using Cnam.TestValidation.Model;

namespace Cnam.TestValidation.WebApi.WebServiceBody
{
    public class RequestPostBodyRequest
    {
        /// <summary>
        /// Description for swagger
        /// </summary>
        /// <example>120eb296-cb23-40ad-a330-c35a8244fde7</example>
        public Guid UserId{ get; set; }
        /// <summary>
        /// Description for swagger
        /// </summary>
        /// <example>d21c8bf9-0b5b-4ffc-b5c7-e977bbb15980</example>
        public Guid RequestedItem { get; set; }
        /// <summary>
        /// Description for swagger
        /// </summary>
        /// <example>2</example>
        public int Quantity { get; set; }
    }
}
