using Cnam.TestValidation.DomainService;
using Cnam.TestValidation.DomainService.CofeeUseCaseException;
using Cnam.TestValidation.Infrastructure;
using Cnam.TestValidation.Model;
using Cnam.TestValidation.WebApi.WebServiceBody;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Text.Json;

namespace Cnam.TestValidation.WebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CofeeManagerUserController : ControllerBase
    {
        private readonly ILogger<CofeeManagerUserController> _logger;
        public IDataExchange postGreInfrastructure = new PostgreInfrastructure();
        public CofeeManagerUserController(ILogger<CofeeManagerUserController> logger)
        {
            _logger = logger;
        }

        /// <summary>
        /// Allow the creation of the user into a database
        /// </summary>
        /// <remarks>Can be used for the creation of a new account</remarks>
        /// <response code="201">Successfull creation of account</response>
        /// <response code="409">Error because of already created user</response>
        /// <response code="400">Error in the request providedr</response>
        /// <response code="500">Something went wrong on the server side</response>
        [HttpPost]
        [Route("user")]
        public IActionResult CreateUser(UserPostBodyRequest user)
        {
            CofeeUseCase cofeeUseCase = new(postGreInfrastructure);
            try
            {
                cofeeUseCase.AddUser(user);
            }
            catch (CofeeUseCaseUserCreationException ex)
            {
                return Conflict($"Already created ressource : {ex}");
            }
            catch (GenericUseCaseException ex)
            {
                return new StatusCodeResult(StatusCodes.Status500InternalServerError);
            }
            return Created("", "Successfull creation of account");
        }

        /// <summary>
        /// Provide guid regarding an user only if data input are correct
        /// </summary>
        /// <response code="200">Correct user provided : return Guid of user</response>
        /// <response code="401">Incorrect user provided</response>
        /// <response code="500">Something went wrong on the server side</response>
        [HttpPost]
        [Route("user/token")]
        public IActionResult GetUserId(UserPostBodyRequest user)
        {
            CofeeUseCase cofeeUseCase = new(postGreInfrastructure);
            Guid guid;
            try
            {
                guid = cofeeUseCase.ProvideGuidOfUser(user.UserName, user.Password);
                return Ok(guid);
            }
            catch (CofeeUseCaseUserErrorUserAuthenthificationException ex)
            {
                return new StatusCodeResult(StatusCodes.Status401Unauthorized);
                /*Dictionary<string, string> errorJson = new Dictionary<string, string>()
                {
                    { "error", $"{ex}"}
                };
                var result = Content(JsonConvert.SerializeObject(errorJson.ToString()));
                result.StatusCode = StatusCodes.Status500InternalServerError;
                result.ContentType = "application/json";
                return result;*/
            }
            catch (CofeeUseCaseUserAccesException ex)
            {
                return NoContent();
            }
            catch (GenericUseCaseException ex)
            {
                return new StatusCodeResult(StatusCodes.Status500InternalServerError);
            }
            catch (Exception ex)
            {
                // to do :log error
                Dictionary<string, string> errorDico = new Dictionary<string, string>()
                {
                    { "err", $"{ex}"}
                };
                string errorJson = JsonConvert.SerializeObject(errorDico);
                var result = Content(errorJson.ToString());
                result.StatusCode = StatusCodes.Status500InternalServerError;
                result.ContentType = "application/json";
                return result;
            }
        }

        /// <summary>
        /// Provide data regarding an user
        /// </summary>
        /// <remarks>Can be used for the creation of a new account</remarks>
        /// <response code="200">User found</response>
        /// <response code="500">Something went wrong on the server side</response>
        [HttpGet]
        [Route("user/{idUser}")]
        public IActionResult GetUser([FromRoute] Guid idUser)
        {
            CofeeUseCase cofeeUseCase = new(postGreInfrastructure);
            try
            {
                return Ok(cofeeUseCase.GetUser(idUser));
            }
            catch (CofeeUseCaseUserAccesException ex)
            {
                return NotFound();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                var result = Content(JsonConvert.SerializeObject(ex), "application/json");
                result.StatusCode = StatusCodes.Status500InternalServerError;
                return result;
            }
        }

        /// <summary>
        /// Provide data regarding an user
        /// </summary>
        /// <remarks>Can be used for the creation of a new account</remarks>
        /// <response code="200">User found</response>
        /// <response code="404">No content found</response>
        /// <response code="500">Something went wrong on the server side</response>
        [HttpGet]
        [Route("user/all")]
        public IActionResult GetAllUser()
        {
            CofeeUseCase cofeeUseCase = new(postGreInfrastructure);
            try
            {
                return Ok(cofeeUseCase.GellAllUser());
            }
            catch (CofeeUseCaseUserAccesException ex)
            {
                return NotFound();
            }
            catch (Exception ex)
            {
                return new StatusCodeResult(StatusCodes.Status500InternalServerError);
            }
        }

        /// <summary>
        /// Provide purse value regarding an user
        /// </summary>
        /// <remarks>Can be used for the purse check of a user</remarks>
        /// <response code="200">Purse value</response>
        /// <response code="404">No content found</response>
        /// <response code="500">Something went wrong on the server side</response>
        [HttpGet]
        [Route("user/{idUser}/purse/")]
        public IActionResult GetPurse([FromRoute] Guid idUser)
        {
            CofeeUseCase cofeeUseCase = new(postGreInfrastructure);
            try
            {
                return Ok(cofeeUseCase.GetUser(idUser).UserPurse.CoinPurse);
            }
            catch (CofeeUseCaseUserAccesException ex)
            {
                return NotFound();
            }
            catch (Exception ex)
            {
                return new StatusCodeResult(StatusCodes.Status500InternalServerError);
            }
        }

        
        /// <summary>
        /// Provide a way to increase the purse value
        /// </summary>
        /// <remarks>Can be used for the purse check of a user</remarks>
        /// <response code="200">Purse increased of the required value</response>
        /// <response code="404">No content found</response>
        /// <response code="400">Error are in the request</response>
        /// <response code="500">Something went wrong on the server side</response>
        [HttpPut]
        [Route("user/{userId}/purse/add/{Value}")]
        public IActionResult IncreasePurse([FromRoute] Guid userId,[FromRoute] decimal Value)
        {
            return UpdatePurse(userId, Value, PurseAction.Add);
        }

        /// <summary>
        /// Provide a way to decrease the purse value
        /// </summary>
        /// <remarks>Can be used for the purse check of a user</remarks>
        /// <response code="200">Purse decrease of the required value</response>
        /// <response code="409">Cannot decrease because of lacking fund</response>
        /// <response code="400">Error are in the request</response>
        /// <response code="404">User not found</response>
        /// <response code="500">Something went wrong on the server side</response>
        [HttpPut]
        [Route("user/{userId}/purse/remove/{Value}")]
        public IActionResult DecreasePurse([FromRoute] Guid userId, [FromRoute] decimal Value)
        {
            return UpdatePurse(userId, Value, PurseAction.Remove);
        }

        private IActionResult UpdatePurse(Guid userId, decimal Value, PurseAction action)
        {
            CofeeUseCase cofeeUseCase = new(postGreInfrastructure);
            User userToUpdate;
            try
            {
                userToUpdate = cofeeUseCase.GetUser(userId);
            }
            catch (CofeeUseCaseUserAccesException ex)
            {
                return NotFound();
            }
            catch (Exception ex)
            {
                return new StatusCodeResult(StatusCodes.Status500InternalServerError);
            }

            try
            {
                cofeeUseCase.ActionOnPurse(action, userToUpdate, Value);
            }
            catch (CofeeUseCaseUserPurseUpdateException ex)
            {
                return Conflict(JsonConvert.SerializeObject(ex));
            }
            catch(CofeeUseCaseValueException ex)
            {
                return BadRequest(JsonConvert.SerializeObject(ex));
            }
            catch (Exception ex)
            {
                return new StatusCodeResult(StatusCodes.Status500InternalServerError);
            }
            return Ok();
        }
    }
}