import { Component, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { Item } from '../model/items-model.model';
import { ItemState } from '../state/item-state';
import { DeleteItem } from '../actions/item-action';
import { UserRequestService } from '../service/user-request.service';
import { UserRequest } from '../model/user-request.model';
import { DataHeaderService } from '../service/data-header.service';
@Component({
  selector: 'app-basket',
  templateUrl: './basket.component.html',
  styleUrls: ['./basket.component.css']
})
export class BasketComponent implements OnInit {

  requests: UserRequest[] ;
  constructor(private store : Store, private userRequestservice:UserRequestService,private sharedData:DataHeaderService) { 
    this.requests = [];
  }
  //requestbasket : Observable<UserRequest[]> | undefined;

  @Select(ItemState.getNbProducts) nb$!: Observable<number>;
  
  @Select(ItemState.getListeProducts) liste$!: Observable<Item[]> ;
  
  DeleteItem(id : Item) {
    this.store.dispatch(new DeleteItem(id));
    console.log(id);
  }
  ngOnInit(): void {
    this.userRequestservice.GetUserRequest().subscribe((requests: UserRequest[]) => {this.requests = requests;});
    
  }

}
