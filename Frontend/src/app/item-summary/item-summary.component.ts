import { Component, OnInit , Input , Output , EventEmitter } from '@angular/core';
import { Subscription } from 'rxjs';
import { Item } from '../model/items-model.model';
import { CreditingRequestService } from '../service/crediting-request.service';
import { DataHeaderService } from "../service/data-header.service";
import { Store } from '@ngxs/store';
import { ItemsServiceService } from '../service/items-service.service';
import { Router } from '@angular/router';
import { AddItem } from '../actions/item-action';
@Component({
  selector: 'app-item-summary',
  templateUrl: './item-summary.component.html',
  styleUrls: ['./item-summary.component.css']
})

export class ItemSummaryComponent implements OnInit {
  public quantity:number=0;

  public showSendButton: boolean = true;
  public ShowLoadingButton: boolean = false;
  public ShowSuccesMessage: boolean = false;
  public ShowErrorMessage: boolean = false;

  items: Item[] | undefined;
  ProductList: Item[]= [];
  Products: Subscription | undefined;
  //constructor(private itemsService: ItemsServiceService, private store:Store) {this.items = [];}
  public messageToShow: string = "";

  public responeToCreditingRequest = {
    201: "Succefull request creation",
    400: "Error are in the request (negativ quantity)",
    404: "User or Item unknow",
    409: "Cannot make a request : not enought money"
  }

  @Input() item: Item = new Item("","","","");

  constructor(private creditingService:CreditingRequestService,private sharedData:DataHeaderService,private itemsService: ItemsServiceService,private store:Store,private router: Router ) {
    this.sharedData.currentMessage.subscribe(message => "" );
  }

  ngOnInit(): void {
    this.items = [];
  }

  AddItem() {
    this.quantity++;
  }

  RemoveItem(){
    if (this.quantity > 0) this.quantity--;
  }
  SendthisItem(item: Item) {
    this.store.dispatch(new AddItem(item));
    console.log(item);
    
  }
  SendButtonCommand(){
    if (this.quantity ===0) return;
    this.LoadingButtonActivate();
    const idUser = (window.sessionStorage.getItem("token") || '')
    this.showSendButton = false;
    this.ShowLoadingButton = true;
    this.SendthisItem(this.item);
    this.creditingService.CreateRequest(idUser,this.item.id,this.quantity).subscribe(
      (response) => {
        this.messageToShow = this.responeToCreditingRequest[201];
        this.SuccesMessageActivate();
        this.SendCommandButtonActivate();
        this.quantity = 0;
        
        this.sharedData.UpdateHeaderValue("");
      },
      (other) => {
          if (other.status == 400) this.messageToShow = this.responeToCreditingRequest[400];
          if (other.status == 404) this.messageToShow = this.responeToCreditingRequest[404];
          if (other.status == 409) this.messageToShow = this.responeToCreditingRequest[409];
          this.ErrorMessageActivate()
          this.SendCommandButtonActivate()
      },
      () =>{

      }
    )
    
  }

  private LoadingButtonActivate(){
    this.showSendButton = false;
    this.ShowLoadingButton = true;
  }
  private SendCommandButtonActivate(){
    this.showSendButton = true;
    this.ShowLoadingButton = false;
  }

  private SuccesMessageActivate(){
    this.ShowSuccesMessage = true;
    this.ShowErrorMessage = false;
  }

  private ErrorMessageActivate(){
    this.ShowSuccesMessage = false;
    this.ShowErrorMessage = true;
  }


}
