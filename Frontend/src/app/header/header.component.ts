import { Component, OnInit} from '@angular/core';
import { DataHeaderService } from '../service/data-header.service';
import { UserPurseService }from '../service/user-purse.service';
import { CreditingRequestService } from '../service/crediting-request.service';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { Router } from "@angular/router";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  searchText: any;
  public notAuthentified:boolean= true;
  public purseValue:any;
  public inProgressRequest: number = 0;

  message:string="";
  constructor(private sharedData:DataHeaderService, private purseService:UserPurseService,private creditingRequestService:CreditingRequestService, private router:Router ) {
    if ( ! window.sessionStorage.getItem("token")){
      this.DisableUserData(true);
      this.purseValue=0;
      this.inProgressRequest = 0;
    }
    else {
      this.DisableUserData(false);
      this.purseValue=0;
   }
 }

   DisableUserData(input:boolean){
     this.notAuthentified = input
   }

  ngOnInit(): void {
    this.sharedData.currentMessage.subscribe(message => {
      this.RefreshHeaderData();
    })
  }

  RefreshHeaderData(){
    this.RefreshPureValue()
    this.UserIsALreadyLogin();
    this.RefreshNumberOgInProgressRequest();
  }

  public Logout(){
    window.sessionStorage.setItem("token","");
    this.RefreshHeaderData();
  }


  private RefreshPureValue(){
    this.purseService.GetPurseValue()
    .subscribe(
        (response) => {                           //next() callback
          console.log('response received')
          this.purseValue = response;
        },
        (error) => {                              //error() callback
          console.error('Request failed with error');
          //if(error.status == 401) this.badLoginOrPassword = true;
          //else window.alert("Please try later, somthing went wrong");
          //console.log(JSON.stringify(error));
          //this.errorMessage = error
        },
        () => {                                   //complete() callback
          console.error('Request completed')      //This is actually not needed
        })
  }

  private UserIsALreadyLogin(){
    if (window.sessionStorage.getItem("token")) this.DisableUserData(false);
  }

  private RefreshNumberOgInProgressRequest(){
    let idUser = window.sessionStorage.getItem("token") || ""
    this.creditingRequestService.AccesRequestInProgress(idUser).subscribe(
      (response) => {
        this.inProgressRequest = response.length;
      },
      (tmp) => {
        console.error(tmp)
      },
      () => {}
    )
  }



}
