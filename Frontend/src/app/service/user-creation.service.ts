import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http'
import { Observable } from 'rxjs';


const headers = new HttpHeaders()
            .set("Access-Control-Allow-Origin", "*")
            .set('Content-Type','application/json');

@Injectable({
  providedIn: 'root'
})
export class UserCreationService {

  constructor(private http:HttpClient) {
  }

  AccountCreation(login:string,password:string): Observable<string> {
    console.log("login: ${login}, password: ${password}");
    const url="https://localhost:7210/api/CofeeManagerUser/user";
    var body = {
                  "userName": login,
                  "password": password,
                };
    console.log(body);
    return this.http
      .post<any>(url, body, {
        headers: headers
        //params: params,
      });//.subscribe((res) => console.log(res));
  }
}
