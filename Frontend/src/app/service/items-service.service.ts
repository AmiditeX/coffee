import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";
import { Item, ItemsAdapter } from '../model/items-model.model';
import { map } from 'rxjs';
import { Subject } from 'rxjs';
const headers = new HttpHeaders()
            .set("Access-Control-Allow-Origin", "*");

@Injectable({
  providedIn: 'root'
})

export class ItemsServiceService {

  public SubjectProducts = new Subject<Item[]>();
  SubjectProducts$: any;
  public item: Item []= [];



  constructor(private http: HttpClient,private adapter: ItemsAdapter) { }
  list(): Observable<Item[]> {
    const url="https://localhost:7210/api/CofeeManagerItems/item/all";
    return this.http.get(url).pipe(
      map((data: any) => data.map((item: any) => this.adapter.adapt(item)))
    )
  }
}
