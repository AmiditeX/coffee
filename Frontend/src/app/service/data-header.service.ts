import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataHeaderService {
  private messageSource = new BehaviorSubject('first message');

  currentMessage = this.messageSource.asObservable();

  constructor() { }

  UpdateHeaderValue(message: string){
      this.messageSource.next(message)
  }

  UpdateFilter(message: string){this.messageSource.next(message)}
}
