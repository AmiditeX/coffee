import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders,HttpParams } from '@angular/common/http'
import { Observable } from 'rxjs';
import { UserRequest } from '../model/user-request.model';
import { Status } from '../model/admin-request.model';

const headers = new HttpHeaders()
            .set("Access-Control-Allow-Origin", "*")
            .set('Content-Type','application/json');

@Injectable({
  providedIn: 'root'
})
export class RequestUpdateService {

  constructor(private http:HttpClient) { }

  UpdatePurse(request:UserRequest,status:Status){
    //const parameter = new HttpParams({fromString: 'status='+status});
    const url="https://localhost:7210/api/CofeeManagerRequest/request/"+ request.id + "?"+'status='+status;
    return this.http
      .put<any>(url,{
        headers: headers,
        responseType: 'text'
        //params: parameter
      })
  }
}
