import { Injectable } from '@angular/core';
import { Action,Selector, State, StateContext } from '@ngxs/store';
import { AddItem,DeleteItem } from '../actions/item-action';
import { ItemStateModel } from './item-state-model';

@State<ItemStateModel>({
  name: 'items',
  defaults: {
    items: [],
  },
})
@Injectable()
export class ItemState {
  @Selector()
  static getNbProducts(state: ItemStateModel) {
    return state.items.length;}
  @Selector()
  static getListeProducts(state: ItemStateModel) {
    return state.items;
  }

  @Action(AddItem)
  add(
    { getState, patchState }: StateContext<ItemStateModel>,
    {payload}: AddItem 
  ) {
    const state = getState();
    patchState({
      items: [...state.items, payload],
    });
  }
 
  @Action(DeleteItem)
  delete(
    { getState, patchState }: StateContext<ItemStateModel>,
    {payload}: DeleteItem
  ) {
    const state = getState();
    patchState({
        items: state.items.filter(p=>p!=payload),
    });
  }
}