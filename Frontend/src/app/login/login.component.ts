import { Component, OnInit } from '@angular/core';
import { UserLoginService } from "../service/user-login.service";
import { DataHeaderService } from "../service/data-header.service";
import { Router } from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public userLogin:string;
  public userPassword:string;
  public badLoginOrPassword: boolean;
  errorMessage:string = "";
  public token:string = "";
  public message:string="";
  public correctLogin:boolean=false;

  ngOnInit(): void{
    this.sharedData.currentMessage.subscribe(message => "" );
    if( window.sessionStorage.getItem("token") ) this.router.navigate(['/main']);
  }

  constructor(private loginService:UserLoginService,private sharedData:DataHeaderService ,private router: Router) {
    this.userLogin = "";
    this.userPassword = "";
    this.badLoginOrPassword = false;
  }

  TryLogin(){
    if(this.loginService.IsAdminAuthentification(this.userLogin,this.userPassword)) this.router.navigate(['/admin']);
    this.loginService.Authentification(this.userLogin,this.userPassword)
    .subscribe(
        (response) => {                           //next() callback
          console.log('response received')
          this.token = response
          window.sessionStorage.setItem("token", this.token );
          this.sharedData.UpdateHeaderValue("");
          this.router.navigate(['/main']);
        },
        (error) => {                              //error() callback
          console.error('Request failed with error');
          if(error.status == 401) this.badLoginOrPassword = true;
          else window.alert("Please try later, somthing went wrong");
          console.log(JSON.stringify(error));
          this.errorMessage = error
        },
        () => {                                   //complete() callback
          //console.error('Request completed')      //This is actually not needed
        })
  }
}
