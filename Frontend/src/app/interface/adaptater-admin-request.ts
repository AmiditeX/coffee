export interface AdaptaterAdminRequest<T> {
  adapt(request: any): T;
}
