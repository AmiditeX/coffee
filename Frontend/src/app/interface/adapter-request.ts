export interface AdapterRequest<T> {
    adapt(request: any): T;
}
