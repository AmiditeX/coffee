export interface AdapterItem<T> {
    adapt(item: any): T;
}
