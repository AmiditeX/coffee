import { AdapterRequest } from "../interface/adapter-request";
import { Injectable } from "@angular/core";
import { Item } from './items-model.model'

export class UserRequest {
  constructor(
    public id: string,
    public item: Item,
    public quantity:number,
    public status: string
  ){
  }
}

@Injectable({
    providedIn: "root",
  })
  export class UserRequestAdapter implements AdapterRequest<UserRequest> {
    adapt(i: any): UserRequest {
    return new UserRequest(i.guid, i.item ,i.requestedQuantity,i.requestStatus);
    }
  }
  