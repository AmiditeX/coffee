import { AdapterUser,AdapterUserData } from "../interface/adapter-user";
import { Injectable } from "@angular/core";

export class UserModel {
  public userName:string =""
  constructor(
    public id: string,
    public purse: number){

  }
}

@Injectable({
    providedIn: "root",
  })
  export class UserAdapter implements AdapterUser<UserModel>,AdapterUserData<UserModel> {
    adapt(i: any): UserModel {
    return new UserModel(i.Id, i.Purse);
    }

    adaptUser(i : any): UserModel {
      let user:UserModel = new UserModel(i.guid,i.userPurse.coinPurse);
      user.userName = i.userName;
      return user;
    }
  }
