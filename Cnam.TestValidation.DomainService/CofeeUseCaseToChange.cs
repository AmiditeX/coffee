﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Cnam.TestValidation.DomainService
{
    public class CofeeUseCaseToChangeXXX : Exception
    {
        public CofeeUseCaseToChangeXXX()
        {
        }

        public CofeeUseCaseToChangeXXX(string? message) : base(message)
        {
        }

        public CofeeUseCaseToChangeXXX(string? message, Exception? innerException) : base(message, innerException)
        {
        }

        protected CofeeUseCaseToChangeXXX(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
