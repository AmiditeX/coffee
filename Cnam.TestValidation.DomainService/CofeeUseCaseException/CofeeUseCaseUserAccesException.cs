﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cnam.TestValidation.DomainService.CofeeUseCaseException
{
    public class CofeeUseCaseUserAccesException : GenericUseCaseException
    {
        public CofeeUseCaseUserAccesException(string? message) : base(message)
        {
        }
    }
}
